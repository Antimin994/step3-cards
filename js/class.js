class Modal {
  constructor(id, title) {
    this.id = id;
    this.title = title;

    this.modalElement = document.getElementById(id);
  }

  open() {
    this.modalElement.style.display = 'block';
  }

  close() {
    this.modalElement.style.display = 'none';
  }
}

class Visit {
  constructor(doctor, purpose, description, urgency, name) {
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
  }

  // Додайте методи для відображення, редагування та видалення візиту
}

class VisitDentist extends Visit {
  constructor(doctor, purpose, description, urgency, name, lastVisitDate) {
    super(doctor, purpose, description, urgency, name);
    this.lastVisitDate = lastVisitDate;
  }
}

class VisitCardiologist extends Visit {
  constructor(doctor, purpose, description, urgency, name, bloodPressure, bmi, heartDiseases, age) {
    super(doctor, purpose, description, urgency, name);
    this.bloodPressure = bloodPressure;
    this.bmi = bmi;
    this.heartDiseases = heartDiseases;
    this.age = age;
  }
}

class VisitTherapist extends Visit {
  constructor(doctor, purpose, description, urgency, name, age) {
    super(doctor, purpose, description, urgency, name);
    this.age = age;
  }
}

export { Modal, Visit, VisitDentist, VisitCardiologist, VisitTherapist };
