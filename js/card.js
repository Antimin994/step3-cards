'use strict';

import { Modal } from './class.js';
import { deleteCard, updateCard } from './api.js';

export class Card {
  constructor(visit) {
    this.visit = visit;
    this.card = document.createElement('div');
    this.card.classList.add('card');
    this.card.style.backgroundColor = "aquamarine"
    this.card.style.width = '350px';

    this.card.classList.add('visit-card');

    this.boundEdit = this.edit.bind(this);
    this.boundExpand = this.expand.bind(this);
    this.boundDelete = this.delete.bind(this);

    this.render();
  }

  render() {
    this.card.innerHTML = `
      <span class="close deleteIcon" style="float: right; cursor: pointer">&times;</span>
      <br>
      <h3>Visit Details</h3>
      <p>Doctor: ${this.visit.doctor}</p>
      <p>Full Name: ${this.visit.name}</p>
      <div class="moreInfo" style="display: none;"></div>
      
      <button class="btn btn-primary" id="showMoreButton" style="margin-bottom: 5px;">Show more</button>
      <button class="btn btn-primary editButton" style="margin-bottom: 5px;">Edit</button>
    `;

    this.expand();

    this.mainCard = document.getElementById('mainCard');

    if (this.mainCard) {
      this.mainCard.appendChild(this.card);
    }

    this.attachEventListeners();
  }

  edit() {
    console.log('edit', this.visit);

    if (typeof this.visit.id === 'undefined') {
      console.error("Invalid visit object: Missing 'id'");
      return;
    }

    const modal = new Modal('cardModal', 'Edit Visit');

    const visitEdit = document.getElementById('saveButton');
    const visitSubmit = document.getElementById('visitSubmit');
    const visitClose = document.getElementById('modalClose');

    if (visitEdit && visitSubmit) {
      visitEdit.style.display = 'block';
      visitSubmit.style.display = 'none';
    }

    // visitEdit.addEventListener('click', () => {
    //   visitEdit.style.display = 'none';
    //   visitSubmit.style.display = 'block';
    // });

    visitClose.addEventListener('click', () => {
      visitEdit.style.display = 'none';
      visitSubmit.style.display = 'block';
    });

    const area = document.getElementById('myArea');

    area.onclick = function () {
      visitEdit.style.display = 'none';
      visitSubmit.style.display = 'block';
    };

    visitEdit.addEventListener('click', () => {
      console.log('visitEdit', this.visit.id);

      const doctorSelect = document.getElementById('doctorSelect');
      const purpose = document.getElementById('purpose');
      const description = document.getElementById('description');
      const urgency = document.getElementById('urgency');
      const fullName = document.getElementById('fullName');
      const bloodPressure = document.getElementById('bloodPressure');
      const bmi = document.getElementById('bmi');
      const cardiovascularHistory = document.getElementById('cardiovascularHistory');
      const patientAge = document.getElementById('patientAge');
      const age = document.getElementById('age');
      const lastVisitDate = document.getElementById('lastVisitDate');

      const updatedData = {
        doctor: doctorSelect.value,
        purpose: purpose.value,
        description: description.value,
        urgency: urgency.value,
        name: fullName.value,
        bloodPressure: bloodPressure.value,
        bmi: bmi.value,
        cardiovascularHistory: cardiovascularHistory.value,
        patientAge: patientAge.value,
        age: age.value,
        lastVisitDate: lastVisitDate.value
      };

      this.visit.doctor = updatedData.doctor;
      this.visit.purpose = updatedData.purpose;
      this.visit.description = updatedData.description;
      this.visit.urgency = updatedData.urgency;
      this.visit.name = updatedData.name;
      this.visit.bloodPressure = updatedData.bloodPressure;
      this.visit.bmi = updatedData.bmi;
      this.visit.cardiovascularHistory = updatedData.cardiovascularHistory;
      this.visit.patientAge = updatedData.patientAge;
      this.visit.age = updatedData.age;

      this.updateVisit(this.visit.id, updatedData);

      modal.close();
    });

    const formElements = {
      doctorSelect: 'doctor',
      purpose: 'purpose',
      description: 'description',
      urgency: 'urgency',
      fullName: 'name',
      bloodPressure: 'bloodPressure',
      bmi: 'bmi',
      cardiovascularHistory: 'cardiovascularHistory',
      patientAge: 'patientAge',
      age: 'age',
      lastVisitDate: 'lastVisitDate'
    };

    for (const elementId in formElements) {
      const element = document.getElementById(elementId);
      if (element) {
        element.value = this.visit[formElements[elementId]];
      }
    }

    modal.open();
  }

  updateVisit(visitId, updatedData) {
    updateCard(visitId, updatedData).then(visitNewData => {
      console.log('card updated', visitId, visitNewData);

      this.visit = visitNewData;

      // Remove old card
      this.card.remove();

      // Render new card
      this.render();
    });
  }

  expand() {
    this.moreInfo = document.createElement('div');
    this.moreInfo.classList.add('moreInfo');
    this.moreInfo.style.backgroundColor = 'greenyellow';
    this.moreInfo.style.display = 'none';
    this.card.appendChild(this.moreInfo);

    const showMoreButton = this.card.querySelector('#showMoreButton');
    const moreInfo = this.card.querySelector('.moreInfo');

    if (showMoreButton && moreInfo) {
      showMoreButton.addEventListener('click', () => {
        if (moreInfo.style.display === 'none' || moreInfo.style.display === '') {
          if (this.visit.doctor === 'Dentist') {
            moreInfo.innerHTML = `
              <p>Purpose: ${this.visit.purpose}</p>
              <p>Description: ${this.visit.description}</p>
              <p>Urgency: ${this.visit.urgency}</p>
              <p>lastVisitDate: ${this.visit.lastVisitDate}</p>
            `;
          } else if (this.visit.doctor === 'Therapist') {
            moreInfo.innerHTML = `
              <p>Purpose: ${this.visit.purpose}</p>
              <p>Description: ${this.visit.description}</p>
              <p>Urgency: ${this.visit.urgency}</p>
              <p>Аge: ${this.visit.patientAge}</p>
            `;
          } else if (this.visit.doctor === 'Cardiologist') {
            moreInfo.innerHTML = `
              <p>Purpose: ${this.visit.purpose}</p>
              <p>Description: ${this.visit.description}</p>
              <p>Urgency: ${this.visit.urgency}</p>
              <p>Normal pressure: ${this.visit.bloodPressure}</p>
              <p>Body mass index: ${this.visit.bmi}</p>
              <p>Transferred diseases of the cardiovascular system: ${this.visit.cardiovascularHistory}</p>
              <p>Age: ${this.visit.age}</p>
            `;
          } else {
            console.error('Unknown doctor type selected');
            return;
          }
          moreInfo.style.display = 'block';
          showMoreButton.textContent = 'Collapse';
        } else {
          moreInfo.innerHTML = '';
          moreInfo.style.display = 'none';
          showMoreButton.textContent = 'Show more';
        }
      });
    }
  }

  delete() {
    deleteCard(this.visit.id).then(() => {
      console.log('card deleted', this.visit.id);

      this.card.remove();
    });
  }

  attachEventListeners() {
    const editButton = this.card.querySelector('.editButton');
    const showMoreButton = this.card.querySelector('#showMoreButton');
    const deleteIcons = this.card.querySelectorAll('.deleteIcon');

    if (editButton) {
      editButton.addEventListener('click', this.boundEdit);
    }

    if (showMoreButton) {
      showMoreButton.addEventListener('click', this.boundExpand);
    }

    if (deleteIcons) {
      deleteIcons.forEach(deleteIcon => {
        deleteIcon.addEventListener('click', this.boundDelete);
      });
    }
  }
}
