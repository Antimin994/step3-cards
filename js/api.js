

const apiURL = 'https://ajax.test-danit.com/api/v2/cards';

const loginURL = `${apiURL}/login`;

export const TOKEN_KEY = 'token';


export function getToken() {
  return localStorage.getItem(TOKEN_KEY);
}

export function getTokenHeader() {
  return `Bearer ${getToken()}`;
}

export function getCards() {

  return fetch(apiURL, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenHeader()
    }
  })
    .then(response => response.json())
    .catch(error => {
      console.error('Error sending request to server', error);

      throw error;
    });
}

export function createCard(data) {
  return fetch(apiURL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenHeader()
    },
    body: JSON.stringify(data)
  })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Server responded with an error');
      }
    })
    .catch(error => {
      console.error('Error sending request to server', error);
    });
}


const initialVisits = [];

export function updateCard(visitId, updatedData) {
  const updateURL = `https://ajax.test-danit.com/api/v2/cards/${visitId}`;

  return fetch(updateURL, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenHeader()
    },
    body: JSON.stringify(updatedData)
  })
    .then(response => response.json())
    .catch(error => {
      console.error('Error sending update request to the server', error);
    });
}

export function deleteCard(visitId) {
  const deleteURL = `https://ajax.test-danit.com/api/v2/cards/${visitId}`;

  return fetch(deleteURL, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: getTokenHeader()
    }
  })
    .then(response => {
      if (response.ok) {
        console.log('Data has been successfully deleted on the server');
        return true;
      } else {
        console.error('Error deleting data on the server');
        throw new Error('Error deleting data on the server');
      }
    })
    .catch(error => {
      console.error('Error deleting data from the server', error);
      // Handle the error, e.g., return a rejected promise or display a message to the user.
      throw error;
    });
}

export function login(data) {
  console.log('login', data);

  return fetch(loginURL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(response => response.text())
    .then(token => {
      localStorage.setItem(TOKEN_KEY, token);
    })
    .catch(error => {
      console.log('Error sending request to server', error);

      throw error;
    });
}
