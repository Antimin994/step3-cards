'use strict';

import { Modal, VisitDentist, VisitCardiologist, VisitTherapist } from './class.js';
import { Card } from './card.js';
import { createCard } from './api.js';

// модальне вікно запису //

document.addEventListener('DOMContentLoaded', function () {
  const openModalBtn = document.getElementById('openModalBtn');
  const closeBtn = document.getElementById('modalClose');
  const doctorSelect = document.getElementById('doctorSelect');
  const visitForm = document.getElementById('visitForm');
  const commonFields = document.getElementById('commonFields');
  const cardiologistFields = document.getElementById('cardiologistFields');
  const dentistFields = document.getElementById('dentistFields');
  const therapistFields = document.getElementById('therapistFields');
  const modal = new Modal('cardModal', 'Modal Title');

  openModalBtn.addEventListener('click', function () {
    modal.open();
  });

  closeBtn.addEventListener('click', function () {
    modal.close();
  });

  window.addEventListener('click', function (event) {
    if (event.target === modal.modalElement) {
      modal.close();
    }
  });

  let visit;

  visitForm.addEventListener('submit', function (e) {
    e.preventDefault();

    const editButton = document.getElementById('saveButton');

    if (editButton && editButton.style.display !== 'none') {
      // skip create new visit, update existing one (From card.js)
      return;
    }

    const meta = document.getElementById('purpose').value;
    const description = document.getElementById('description').value;
    const urgency = document.getElementById('urgency').value;
    const name = document.getElementById('fullName').value;
    const selectedDoctor = doctorSelect.value;

    if (selectedDoctor === 'Dentist') {
      const lastVisitDate = document.getElementById('lastVisitDate').value;
      visit = new VisitDentist(selectedDoctor, meta, description, urgency, name, lastVisitDate);
    } else if (selectedDoctor === 'Therapist') {
      const patientAge = document.getElementById('patientAge').value;
      visit = new VisitTherapist(selectedDoctor, meta, description, urgency, name, patientAge);
    } else if (selectedDoctor === 'Cardiologist') {
      const bloodPressure = document.getElementById('bloodPressure').value;
      const bodyMassIndex = document.getElementById('bmi').value;
      const cardiovascularHistory = document.getElementById('cardiovascularHistory').value;
      const age = document.getElementById('age').value;
      visit = new VisitCardiologist(
        selectedDoctor,
        meta,
        description,
        urgency,
        name,
        bloodPressure,
        bodyMassIndex,
        cardiovascularHistory,
        age
      );
    } else {
      console.error('Unknown doctor type selected');
      return;
    }

    createCard(visit).then(data => {
      console.log('Card.createCard', data);

      new Card(data);

      modal.close();
    });
  });

  doctorSelect.addEventListener('change', function () {
    const selectedDoctor = doctorSelect.value;

    // Спочатку приховуємо всі додаткові поля
    commonFields.classList.add('hidden');
    cardiologistFields.classList.add('hidden');
    dentistFields.classList.add('hidden');
    therapistFields.classList.add('hidden');

    // Показуємо поля, характерні для обраного лікаря
    commonFields.classList.remove('hidden');
    if (selectedDoctor === 'Cardiologist') {
      cardiologistFields.classList.remove('hidden');
    } else if (selectedDoctor === 'Dentist') {
      dentistFields.classList.remove('hidden');
    } else if (selectedDoctor === 'Therapist') {
      therapistFields.classList.remove('hidden');
    }
  });
});
