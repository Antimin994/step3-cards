import { Card } from './card.js';
import './modal.js';
import './card.js';
import { getCards } from './api.js';

let initialVisits = [];
/*initialVisits.then(data => {
  let result = data;
  return result;
})*/

console.log(initialVisits);

let filteredVisits = [];

let searchText = '';


function renderFilteredCards(filteredVisits) {
    mainCard.innerHTML = ''; 
    filteredVisits.forEach(visit => {
      const card = new Card(visit); 
      mainCard.appendChild(card.card); 
    });
  }
  
  function displayFilteredVisits() {

    getCards().then((data) => {

      initialVisits = data;
      console.log(initialVisits);

    const searchInput = document.getElementById('searchInput');
    const statusFilter = document.getElementById('statusFilter');
    const priorityFilter = document.getElementById('priorityFilter');  

    let searchText = searchInput.value.toLowerCase();
    console.log(searchText);
    let selectedStatus = statusFilter.value;
    console.log(selectedStatus);
    let selectedPriority = priorityFilter.value;
    console.log(selectedPriority);
  
    filteredVisits = initialVisits.filter(visit => {
      const titleAndDescription = (visit.name || '').toLowerCase() + (visit.description || '').toLowerCase();
      console.log(titleAndDescription);
      return (
        titleAndDescription.includes(searchText) &&
        (selectedStatus === 'all' || visit.status === selectedStatus) &&
        (selectedPriority === 'all' || visit.urgency === selectedPriority)
      );
      //return (selectedPriority === 'all' || visit.urgency === selectedPriority);
    });

    console.log(filteredVisits);
  
    renderFilteredCards(filteredVisits);

  })
  }

document.addEventListener('DOMContentLoaded', function () {
  

  searchInput.addEventListener('input', displayFilteredVisits);
  statusFilter.addEventListener('change', displayFilteredVisits);
  priorityFilter.addEventListener('change', displayFilteredVisits);

  searchInput.addEventListener('input', function () {
    console.log(searchText);
    displayFilteredVisits();
  });
  statusFilter.addEventListener('change', function () {
    console.log(statusFilter.value);
    displayFilteredVisits();
  });
  priorityFilter.addEventListener('change', function () {
    console.log(priorityFilter.value);
    displayFilteredVisits();
  });
  displayFilteredVisits();
});


