'use strict';
import './api.js';
import './filter.js'
import './card.js';
import './modal.js';
import './class.js';

import { Card } from './card.js';

import { getCards, login, TOKEN_KEY } from './api.js';

import { isValidEmail, isValidPassword } from './password.js';

function renderCardsPage() {
  console.log('renderCardsPage');

  const btnEnter = document.getElementById('btn-enter');
  const btnSignOut = document.getElementById('btn-signOut');
  const filterForm = document.getElementById('filterForm');
  const btnCreateVisit = document.getElementById('openModalBtn');
  const formPassword = document.getElementById('form-password');
  const errorMessage = document.getElementById('error-message');

  btnEnter.style.display = 'none';
  formPassword.style.display = 'none';
  btnCreateVisit.style.display = 'block';
  btnSignOut.style.display = 'block';
  filterForm.style.display = 'flex';
  errorMessage.style.display = 'none';

  getCards().then(data => {
    console.log('render cards', data);

    data.forEach(visit => {
      new Card(visit);
    });

    updateMessageVisibility();
  });
}

function updateMessageVisibility() {
  const cardList = document.getElementById('mainCard');
  const noItemsMessage = document.getElementById('no-items-message');

  if (cardList.children.length === 0) {
    noItemsMessage.style.display = 'block';
  } else {
    noItemsMessage.style.display = 'none';
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const btnEnter = document.getElementById('btn-enter');
  const btnSignOut = document.getElementById('btn-signOut');
  const formPassword = document.getElementById('form-password');

  const token = localStorage.getItem('token');

  if (token) {
    console.log('token exists', token);

    renderCardsPage();
  }

  btnSignOut.addEventListener('click', () => {
    localStorage.removeItem(TOKEN_KEY);
    location.reload();
  });

  btnEnter.addEventListener('click', () => {
    formPassword.style.display = 'block';

    const errorMessage = document.getElementById('error-message');

    formPassword.addEventListener('submit', function (event) {
      event.preventDefault();

      const email = document.getElementById('exampleInputEmail1').value;
      const password = document.getElementById('exampleInputPassword1').value;

      if (isValidEmail(email) && isValidPassword(password)) {
        login({ email, password })
          .then(() => {
            alert('You are successfully logged in!');

            renderCardsPage();
          })
          .catch(() => {
            alert('Something went wrong!');
          });
      } else {
        errorMessage.textContent = 'Invalid email or password.';
      }
    });
  });
});
